import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';

import IssueTile from '../../src/components/IssueTile';
import { ROUTES } from '../../src/navigation/types';
import theme from '../../src/theme/LightTheme';

const mockNavigate = jest.fn();
const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
  useTheme: jest.fn(() => mockTheme),
}));

const mockIssue = {
  id: '1234',
  iid: 'iid 1234',
  milestone: {
    title: 'Milestone 1234',
  },
  state: 'opened' as const,
  taskCompletionStatus: {
    completedCount: 1,
    count: 4,
  },
  title: 'Title 1234',
};

describe('IssueTile', () => {
  it('should render issue data', () => {
    const { getByText } = render(
      <IssueTile fullPath={'FullPath1234'} issue={mockIssue} />,
    );
    expect(getByText(mockIssue.milestone.title)).toBeTruthy();
    expect(getByText(mockIssue.state)).toBeTruthy();
    expect(getByText(mockIssue.title)).toBeTruthy();
    expect(
      getByText(
        mockIssue.taskCompletionStatus.completedCount +
          '/' +
          mockIssue.taskCompletionStatus.count,
      ),
    ).toBeTruthy();
  });
  it('should call navigate method with params', () => {
    const fullPath = 'FullPath1234';
    const { getByA11yLabel } = render(
      <IssueTile fullPath={fullPath} issue={mockIssue} />,
    );
    fireEvent.press(getByA11yLabel(`${mockIssue.title} issue`));
    expect(mockNavigate).toBeCalledWith(ROUTES.ISSUE_DETAILS, {
      fullPath,
      internalId: mockIssue.iid,
    });
  });
});
