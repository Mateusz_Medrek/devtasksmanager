import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';

import Button from '../../src/components/Button';

describe('Button', () => {
  it('should render button with passed title', () => {
    const a11yLabel = 'Button';
    const title = 'MyButton';
    const { getByA11yLabel, getByText } = render(
      <Button {...{ accessibilityLabel: a11yLabel, color: 'white', title }} />,
    );
    expect(getByA11yLabel(a11yLabel)).toBeTruthy();
    expect(getByText(title)).toBeTruthy();
  });
  it('should call onPress', () => {
    const a11yLabel = 'Button';
    const onPress = jest.fn();
    const { getByA11yLabel } = render(
      <Button {...{ accessibilityLabel: a11yLabel, onPress }} />,
    );
    fireEvent.press(getByA11yLabel(a11yLabel));
    expect(onPress).toBeCalledTimes(1);
  });
});
