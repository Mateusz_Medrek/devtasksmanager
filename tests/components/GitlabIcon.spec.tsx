import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';

import GitlabIcon from '../../src/components/GitlabIcon';

describe('GitlabIcon', () => {
  it('should render ImageButton', () => {
    const { queryByA11yRole } = render(<GitlabIcon />);
    expect(queryByA11yRole('imagebutton')).toBeTruthy();
  });
  it('should render disabled ImageButton after passing disabled prop equal to true', () => {
    const { queryByA11yRole, queryByA11yState } = render(
      <GitlabIcon disabled={true} />,
    );
    expect(queryByA11yRole('imagebutton')).toBeTruthy();
    expect(queryByA11yState({ disabled: true })).toBeTruthy();
  });
  it('should call onPress prop after pressing when is not disabled and should not call onPress when is disabled', () => {
    const onPress = jest.fn();
    const { getByA11yRole, rerender } = render(
      <GitlabIcon onPress={onPress} />,
    );
    fireEvent.press(getByA11yRole('imagebutton'));
    expect(onPress).toBeCalledTimes(1);
    onPress.mockClear();
    rerender(<GitlabIcon disabled onPress={onPress} />);
    fireEvent.press(getByA11yRole('imagebutton'));
    expect(onPress).toBeCalledTimes(0);
  });
});
