import { render } from '@testing-library/react-native';
import React from 'react';

import IssueDiscussionNote from '../../src/components/IssueDiscussionNote';
import theme from '../../src/theme/LightTheme';

const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useTheme: jest.fn(() => mockTheme),
}));

const mockNote = {
  author: {
    avatarUrl: null,
    name: 'Mock user',
    username: 'Mock_User',
  },
  body: 'Mocked note',
  createdAt: '2020-01-03',
  id: '1234',
};

describe('IssueDiscussionNote', () => {
  it('should render note.body text', () => {
    const { getByText } = render(
      <IssueDiscussionNote isReply={false} note={mockNote} />,
    );
    expect(getByText(mockNote.body)).toBeTruthy();
  });
});
