import { render } from '@testing-library/react-native';
import React from 'react';

import UserAvatar from '../../src/components/UserAvatar';

describe('UserAvatar', () => {
  it('should render image', () => {
    const { queryByA11yRole, rerender } = render(
      <UserAvatar variant="large" />,
    );
    expect(queryByA11yRole('image')).toBeTruthy();
    rerender(<UserAvatar avatarUrl="avatarUrl" />);
    expect(queryByA11yRole('image')).toBeTruthy();
  });
});
