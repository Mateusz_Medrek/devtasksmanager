import { render } from '@testing-library/react-native';
import React from 'react';

import ProjectMetadata from '../../src/components/ProjectMetadata';
import theme from '../../src/theme/LightTheme';
import defaultTranslations from '../../src/translations/defaultTranslations';

const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useTheme: jest.fn(() => mockTheme),
}));

const mockProject = {
  avatarUrl: null,
  description: 'Description1234',
  fullPath: 'FullPath1234',
  id: '1234',
  name: 'Project1234',
  nameWithNamespace: 'Project1234-Namespace1234',
  openIssuesCount: 1,
  visibility: 'public' as const,
};

describe('ProjectMetadata', () => {
  it('should render project details', () => {
    const { getByText, rerender } = render(
      <ProjectMetadata project={mockProject} />,
    );
    expect(getByText(mockProject.description)).toBeTruthy();
    expect(
      getByText(defaultTranslations.projectDetails.description),
    ).toBeTruthy();
    expect(getByText(mockProject.id)).toBeTruthy();
    expect(getByText(defaultTranslations.projectDetails.id)).toBeTruthy();
    expect(getByText(mockProject.name)).toBeTruthy();
    expect(getByText(mockProject.nameWithNamespace)).toBeTruthy();
    expect(getByText(`${mockProject.openIssuesCount}`)).toBeTruthy();
    expect(
      getByText(defaultTranslations.projectDetails.openIssues),
    ).toBeTruthy();
    rerender(<ProjectMetadata project={{ ...mockProject, description: '' }} />);
    expect(
      getByText(defaultTranslations.projectDetails.noDescription),
    ).toBeTruthy();
  });
});
