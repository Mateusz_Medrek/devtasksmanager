import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';

import Header from '../../src/components/Header';
import theme from '../../src/theme/LightTheme';

const mockCanGoBack = jest.fn(() => true);
const mockGoBack = jest.fn();
const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    canGoBack: mockCanGoBack,
    goBack: mockGoBack,
  })),
  useTheme: jest.fn(() => mockTheme),
}));

jest.mock('react-native-safe-area-context', () => ({
  useSafeAreaInsets: jest.fn(() => ({
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
  })),
}));

describe('Header', () => {
  it('should render back button, should call navigation.goBack method', () => {
    const { getByA11yLabel } = render(<Header />);
    expect(getByA11yLabel('Back button')).toBeTruthy();
    fireEvent.press(getByA11yLabel('Back button'));
    expect(mockGoBack).toBeCalledTimes(1);
  });
});
