import { MockedProvider } from '@apollo/client/testing';
import { render, waitFor } from '@testing-library/react-native';
import React from 'react';

import UserData from '../../src/components/UserData';
import { GET_CURRENT_USER } from '../../src/graphql/queries';

describe('UserData', () => {
  it('should initially show loading indicator', async () => {
    const userData = {
      currentUser: {
        avatarUrl: 'avatarUrl',
        name: 'name',
        username: 'username',
      },
    };
    const mocks = [
      {
        request: {
          query: GET_CURRENT_USER,
        },
        result: {
          data: userData,
        },
      },
    ];
    const { queryAllByA11yRole, queryByText, queryByTestId } = render(
      <UserData />,
      {
        wrapper: ({ children }) => (
          <MockedProvider mocks={mocks}>{children}</MockedProvider>
        ),
      },
    );
    expect(queryByTestId('LoadingIndicatorID')).toBeTruthy();
    expect(queryAllByA11yRole('image').length).not.toBeGreaterThanOrEqual(1);
    expect(queryByText(userData.currentUser.name)).not.toBeTruthy();
    expect(queryByText(`@${userData.currentUser.username}`)).not.toBeTruthy();
  });
  it('should return image, name and username', async () => {
    const userData = {
      currentUser: {
        avatarUrl: 'avatarUrl',
        name: 'name',
        username: 'username',
      },
    };
    const mocks = [
      {
        request: {
          query: GET_CURRENT_USER,
        },
        result: {
          data: userData,
        },
      },
    ];
    const { getAllByA11yRole, getByText } = render(<UserData />, {
      wrapper: ({ children }) => (
        <MockedProvider mocks={mocks}>{children}</MockedProvider>
      ),
    });
    await waitFor(() => true, { timeout: 120 });
    expect(getAllByA11yRole('image').length).toBeGreaterThanOrEqual(1);
    expect(getByText(userData.currentUser.name)).toBeTruthy();
    expect(getByText(`@${userData.currentUser.username}`)).toBeTruthy();
  });
});
