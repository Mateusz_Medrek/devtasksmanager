import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';

import CloseButton from '../../src/components/CloseButton';
import theme from '../../src/theme/LightTheme';

const mockGoBack = jest.fn();
const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    goBack: mockGoBack,
  })),
  useTheme: jest.fn(() => mockTheme),
}));

jest.mock('react-native-reanimated', () =>
  require('react-native-reanimated/mock'),
);

describe('CloseButton', () => {
  it('should call navigation.goBack method', () => {
    const { getByA11yLabel } = render(<CloseButton />);
    expect(getByA11yLabel('Close button')).toBeTruthy();
    fireEvent.press(getByA11yLabel('Close button'));
    expect(mockGoBack).toBeCalledTimes(1);
  });
});
