import { render } from '@testing-library/react-native';
import React from 'react';

import IssueState from '../../src/components/IssueState';
import type { IssueStateValue } from '../../src/types/GitlabTypes';

describe('IssueState', () => {
  it('should render issue state value', () => {
    let value: IssueStateValue = 'opened';
    const { getByText, rerender } = render(
      <IssueState value={value} variant="large" />,
    );
    expect(getByText(value)).toBeTruthy();
    value = 'closed';
    rerender(<IssueState value={value} variant="small" />);
    expect(getByText(value)).toBeTruthy();
    value = 'all';
    rerender(<IssueState value={value} />);
    expect(getByText(value)).toBeTruthy();
    value = 'locked';
    rerender(<IssueState value={value} variant="small" />);
    expect(getByText(value)).toBeTruthy();
  });
});
