import { render } from '@testing-library/react-native';
import React from 'react';

import IssueAssignee from '../../src/components/IssueAssignee';
import theme from '../../src/theme/LightTheme';

const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useTheme: jest.fn(() => mockTheme),
}));

describe('IssueAssignee', () => {
  it('should render assignee text', () => {
    const assigneeText = 'Assignee 1234';
    const { getByText } = render(<IssueAssignee assignee={assigneeText} />);
    expect(getByText(assigneeText)).toBeTruthy();
  });
});
