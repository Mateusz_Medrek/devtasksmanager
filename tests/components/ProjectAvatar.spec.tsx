import { render } from '@testing-library/react-native';
import React from 'react';

import ProjectAvatar from '../../src/components/ProjectAvatar';
import theme from '../../src/theme/LightTheme';

const mockNavigate = jest.fn();
const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
  useTheme: jest.fn(() => mockTheme),
}));

describe('ProjectAvatar', () => {
  it('should render project avatar icon/image', () => {
    const avatarUrl = 'AvatarUrl1234';
    const { getByA11yLabel, rerender } = render(
      <ProjectAvatar
        avatarUrl={avatarUrl}
        borderColor="white"
        variant="large"
      />,
    );
    expect(getByA11yLabel('Project avatar icon'));
    rerender(<ProjectAvatar avatarUrl={null} variant="small" />);
    expect(getByA11yLabel('Project avatar icon'));
    rerender(<ProjectAvatar />);
    expect(getByA11yLabel('Project avatar icon'));
  });
});
