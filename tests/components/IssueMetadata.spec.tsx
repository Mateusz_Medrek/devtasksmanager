import { render } from '@testing-library/react-native';
import React from 'react';

import IssueMetadata from '../../src/components/IssueMetadata';
import theme from '../../src/theme/LightTheme';
import defaultTranslations from '../../src/translations/defaultTranslations';

const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useTheme: jest.fn(() => mockTheme),
}));

const mockIssue = {
  author: {
    avatarUrl: null,
    name: 'Author1234',
    username: 'Author_1234',
  },
  description: 'Description1234',
  id: '1234',
  iid: 'iid_1234',
  milestone: {
    description: 'MilestoneDescription1234',
    title: 'Milestone1234',
  },
  state: 'opened' as const,
  taskCompletionStatus: {
    completedCount: 1,
    count: 4,
  },
  title: 'Issue1234',
};

describe('IssueMetadata', () => {
  it('should render issue metadata', () => {
    const assignees = [
      {
        avatarUrl: null,
        name: 'Assignee1234',
        username: 'Assignee_1234',
      },
    ];
    const { getByText, queryByText, rerender } = render(
      <IssueMetadata assignees={assignees} issue={mockIssue} />,
    );
    expect(getByText(defaultTranslations.issueDetails.assignees)).toBeTruthy();
    assignees.map((assignee) => {
      expect(getByText(assignee.name)).toBeTruthy();
    });
    expect(getByText(defaultTranslations.issueDetails.author)).toBeTruthy();
    expect(getByText(mockIssue.author.name)).toBeTruthy();
    expect(
      getByText(defaultTranslations.issueDetails.description),
    ).toBeTruthy();
    expect(getByText(mockIssue.description)).toBeTruthy();
    expect(getByText(defaultTranslations.issueDetails.milestone)).toBeTruthy();
    expect(getByText(mockIssue.milestone.description)).toBeTruthy();
    expect(getByText(mockIssue.milestone.title)).toBeTruthy();
    expect(getByText(defaultTranslations.issueDetails.tasks)).toBeTruthy();
    expect(
      getByText(
        mockIssue.taskCompletionStatus.completedCount +
          '/' +
          mockIssue.taskCompletionStatus.count,
      ),
    ).toBeTruthy();
    rerender(<IssueMetadata assignees={assignees} />);
    expect(queryByText(defaultTranslations.issueDetails.assignees)).toBeFalsy();
    assignees.map((assignee) => {
      expect(queryByText(assignee.name)).toBeFalsy();
    });
    expect(queryByText(defaultTranslations.issueDetails.author)).toBeFalsy();
    expect(queryByText(mockIssue.author.name)).toBeFalsy();
    expect(
      queryByText(defaultTranslations.issueDetails.description),
    ).toBeFalsy();
    expect(queryByText(mockIssue.description)).toBeFalsy();
    expect(queryByText(defaultTranslations.issueDetails.milestone)).toBeFalsy();
    expect(queryByText(mockIssue.milestone.description)).toBeFalsy();
    expect(queryByText(mockIssue.milestone.title)).toBeFalsy();
    expect(queryByText(defaultTranslations.issueDetails.tasks)).toBeFalsy();
    expect(
      queryByText(
        mockIssue.taskCompletionStatus.completedCount +
          '/' +
          mockIssue.taskCompletionStatus.count,
      ),
    ).toBeFalsy();
  });
});
