import { render } from '@testing-library/react-native';
import React from 'react';

import LoadingIndicator from '../../src/components/LoadingIndicator';

describe('LoadingIndicator', () => {
  it('should render component with testID', () => {
    const { getByTestId } = render(<LoadingIndicator />);
    expect(getByTestId('LoadingIndicatorID')).toBeTruthy();
  });
});
