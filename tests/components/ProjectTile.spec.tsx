import { fireEvent, render } from '@testing-library/react-native';
import React from 'react';

import ProjectTile from '../../src/components/ProjectTile';
import { ROUTES } from '../../src/navigation/types';
import theme from '../../src/theme/LightTheme';

const mockNavigate = jest.fn();
const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
  useTheme: jest.fn(() => mockTheme),
}));

const mockProject = {
  avatarUrl: null,
  fullPath: 'FullPath1234',
  id: '1234',
  name: 'Project1234',
};

describe('ProjectTile', () => {
  it('should render project data', () => {
    const { getByA11yLabel, getByText } = render(
      <ProjectTile project={mockProject} />,
    );
    expect(getByText(mockProject.id)).toBeTruthy();
    expect(getByText(mockProject.name)).toBeTruthy();
    expect(getByA11yLabel('Project avatar icon')).toBeTruthy();
  });
  it('should call navigate method with params', () => {
    const { getByA11yLabel } = render(<ProjectTile project={mockProject} />);
    fireEvent.press(getByA11yLabel(`${mockProject.name} project`));
    expect(mockNavigate).toBeCalledWith(ROUTES.PROJECT_DETAILS, {
      fullPath: mockProject.fullPath,
    });
  });
});
