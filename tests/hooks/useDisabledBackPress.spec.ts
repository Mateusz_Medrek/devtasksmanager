import { renderHook } from '@testing-library/react-hooks';
import { useEffect } from 'react';
import { BackHandler } from 'react-native';

import useDisabledBackPress from '../../src/hooks/useDisabledBackPress';

/**
 * To simulate focus/blur events let's switch to useEffect mount/unmount effects
 * and use unmount function from @testing-library/react-hooks as a replacement
 */
const mockUseFocusEffect = useEffect;

jest.mock('@react-navigation/native', () => ({
  useFocusEffect: jest.fn((callback) => {
    mockUseFocusEffect(() => {
      const cleanup = callback();
      return () => cleanup();
    }, []);
  }),
}));

jest.mock('react-native', () => ({
  BackHandler: {
    addEventListener: jest.fn((_, cb) => cb()),
    removeEventListener: jest.fn(),
  },
}));

describe('useDisabledBackPress', () => {
  afterEach(() => {
    (BackHandler.addEventListener as any).mockClear();
    (BackHandler.removeEventListener as any).mockClear();
  });
  it('should call BackHandler.addEventListener and BackHandler.removeEventListener', () => {
    const { unmount } = renderHook(() => useDisabledBackPress());
    expect(BackHandler.addEventListener).toBeCalledTimes(1);
    unmount();
    expect(BackHandler.removeEventListener).toBeCalledTimes(1);
  });
});
