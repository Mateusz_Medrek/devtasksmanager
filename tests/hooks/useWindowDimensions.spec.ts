import { renderHook } from '@testing-library/react-hooks';
import { Dimensions } from 'react-native';

import { IPHONE11_HEIGHT, IPHONE11_WIDTH } from '../../src/consts/styles';
import useWindowDimensions from '../../src/hooks/useWindowDimensions';

const mockWindow = {
  height: IPHONE11_HEIGHT,
  width: IPHONE11_WIDTH,
};

jest.mock('react-native', () => ({
  Dimensions: {
    addEventListener: jest.fn((_, cb) => cb({ window: mockWindow })),
    get: jest.fn(() => mockWindow),
    removeEventListener: jest.fn(),
  },
}));

describe('useWindowDimensions', () => {
  afterEach(() => {
    (Dimensions.addEventListener as any).mockClear();
    (Dimensions.get as any).mockClear();
    (Dimensions.removeEventListener as any).mockClear();
  });
  it('should call Dimensions.addEventListener and Dimensions.removeEventListener', () => {
    const { unmount } = renderHook(() => useWindowDimensions());
    expect(Dimensions.addEventListener).toBeCalledTimes(1);
    unmount();
    expect(Dimensions.removeEventListener).toBeCalledTimes(1);
  });
  it('should return window size', async () => {
    const { result } = renderHook(() => useWindowDimensions());
    expect(result.current).toBe(mockWindow);
  });
});
