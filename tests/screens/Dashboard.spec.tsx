// Mock components
import React from 'react';
import { View } from 'react-native';
import type { ViewProps } from 'react-native';

const mockModuleWithReactComponent = {
  __esModule: true,
  default: (props: ViewProps) => <View {...props} />,
};

jest.mock(
  '../../src/components/ProjectList',
  () => mockModuleWithReactComponent,
);
jest.mock('../../src/components/UserData', () => mockModuleWithReactComponent);

import { render } from '@testing-library/react-native';

import Dashboard from '../../src/screens/Dashboard';
import theme from '../../src/theme/DarkTheme';

const mockNavigate = jest.fn();
const mockTheme = theme;

jest.mock('@react-navigation/native', () => ({
  useFocusEffect: jest.fn((cb) => cb()),
  useNavigation: jest.fn(() => ({
    navigate: mockNavigate,
  })),
  useRoute: jest.fn(() => ({
    params: {},
  })),
  useTheme: jest.fn(() => mockTheme),
}));

jest.mock('react-native-safe-area-context', () => ({
  useSafeAreaInsets: jest.fn(() => ({
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
  })),
}));

describe('Dashboard', () => {
  it('should render UserData and ProjectList', () => {
    const { getByTestId } = render(<Dashboard />);
    expect(getByTestId('UserDataID')).toBeTruthy();
    expect(getByTestId('ProjectListID')).toBeTruthy();
  });
});
