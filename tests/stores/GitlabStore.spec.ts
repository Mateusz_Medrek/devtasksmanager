import { act, renderHook } from '@testing-library/react-hooks';

import useGitlabStore from '../../src/stores/GitlabStore';

const mockHydratedTokens = {
  accessToken: 'mockAccessToken',
  refreshToken: 'mockRefreshToken',
};

jest.mock('../../src/services/GitlabService', () => ({
  getTokens: jest.fn(() => Promise.resolve(mockHydratedTokens)),
  persistTokens: jest.fn(() => Promise.resolve()),
}));

describe('useGitlabStore', () => {
  it('should return default accessToken and refreshToken', () => {
    const { result } = renderHook(() => useGitlabStore());
    expect(result.current.accessToken).toBe('');
    expect(result.current.refreshToken).toBe('');
  });
  it('should hydrate tokens', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useGitlabStore());
    act(() => {
      result.current.hydrate();
    });
    await waitForNextUpdate({ timeout: 120 });
    expect(result.current.accessToken).toBe(mockHydratedTokens.accessToken);
    expect(result.current.refreshToken).toBe(mockHydratedTokens.refreshToken);
  });
  it('should set tokens', async () => {
    const tokens = {
      access_token: 'access',
      refresh_token: 'refresh',
    };
    const { result } = renderHook(() => useGitlabStore());
    act(() => {
      result.current.setTokens(tokens);
    });
    expect(result.current.accessToken).toBe(tokens.access_token);
    expect(result.current.refreshToken).toBe(tokens.refresh_token);
  });
});
