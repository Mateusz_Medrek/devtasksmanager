// TYPES
type Edge<T> = {
  cursor: string;
  node: T;
};

export type Discussion = {
  createdAt: string;
  id: string;
  notes: {
    edges: Edge<Note>[];
    pageInfo: {
      hasNextPage: boolean;
    };
  };
  resolved: boolean;
};

export type IssueSortValue =
  | 'updated_desc'
  | 'updated_asc'
  | 'created_desc'
  | 'created_asc'
  | 'PRIORITY_DESC'
  | 'PRIORITY_ASC'
  | 'LABEL_PRIORITY_DESC'
  | 'LABEL_PRIORITY_ASC'
  | 'MILESTONE_DUE_DESC'
  | 'MILESTONE_DUE_ASC'
  | 'DUE_DATE_DESC'
  | 'DUE_DATE_ASC'
  | 'RELATIVE_POSITION_ASC'
  | 'WEIGHT_DESC'
  | 'WEIGHT_ASC';

export type IssueStateValue = 'opened' | 'closed' | 'locked' | 'all';

export type Issue = {
  id: string;
  iid: string;
  milestone: {
    title: string;
  } | null;
  state: IssueStateValue;
  taskCompletionStatus: {
    completedCount: number;
    count: number;
  };
  title: string;
};

export type IssueDetails = {
  author: User;
  description: string;
  id: string;
  iid: string;
  milestone: {
    description: string;
    title: string;
  } | null;
  state: IssueStateValue;
  taskCompletionStatus: {
    completedCount: number;
    count: number;
  };
  title: string;
};

export type Note = {
  author: User;
  body: string;
  createdAt: string;
  id: string;
};

export type Project = {
  avatarUrl: string | null;
  id: string;
  fullPath: string;
  name: string;
};

export type ProjectDetails = {
  avatarUrl: string | null;
  description: string | null;
  fullPath: string;
  id: string;
  name: string;
  nameWithNamespace: string;
  openIssuesCount: number;
  visibility: string;
};

export type User = {
  avatarUrl: string | null;
  name: string;
  username: string;
};

// CurrentUser query
export type CurrentUserQueryData = {
  currentUser: User;
};

// Projects query
export type ProjectsQueryData = {
  projects: {
    edges: Edge<Project>[];
    pageInfo: {
      hasNextPage: boolean;
    };
  };
};

export type ProjectsQueryVariables = {
  first: number;
  after: string;
};

// ProjectDetails query
export type ProjectDetailsQueryData = {
  project: ProjectDetails;
};

export type ProjectDetailsQueryVariables = {
  fullPath: string;
};

// ProjectIssues query
export type ProjectIssuesQueryData = {
  project: {
    id: string;
    issues: {
      edges: Edge<Issue>[];
      pageInfo: {
        hasNextPage: boolean;
      };
    };
  };
};

export type ProjectIssuesQueryVariables = {
  fullPath: string;
  first: number;
  after: string;
  sort: IssueSortValue;
};

// ProjectIssueDetails query
export type ProjectIssueDetailsQueryData = {
  project: {
    id: string;
    issue: IssueDetails;
  };
};

export type ProjectIssueDetailsQueryVariables = {
  fullPath: string;
  iid: string;
};

// ProjectIssueAssignees query
export type ProjectIssueAssigneesQueryData = {
  project: {
    id: string;
    issue: {
      assignees: {
        edges: Edge<User>[];
        pageInfo: {
          hasNextPage: boolean;
        };
      };
      id: string;
    };
  };
};

export type ProjectIssueAssigneesQueryVariables = {
  fullPath: string;
  iid: string;
  first: number;
  after: string;
};

// ProjectIssueDiscussions query
export type ProjectIssueDiscussionsQueryData = {
  project: {
    id: string;
    issue: {
      discussions: {
        edges: Edge<Discussion>[];
        pageInfo: {
          hasNextPage: boolean;
        };
      };
      id: string;
    };
  };
};

export type ProjectIssueDiscussionsQueryVariables = {
  fullPath: string;
  iid: string;
  firstDiscussions: number;
  afterDiscussion: string;
  firstNotes: number;
  afterNote: string;
};
