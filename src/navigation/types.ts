import type { RouteProp } from '@react-navigation/native';
import type { NativeStackNavigationProp } from 'react-native-screens/native-stack';

export const ROUTES = {
  WELCOME_SCREEN: 'WELCOME_SCREEN',
  DASHBOARD: 'DASHBOARD',
  PROJECT_DETAILS: 'PROJECT_DETAILS',
  ISSUE_DETAILS: 'ISSUE_DETAILS',
} as const;

export type RootParamList = {
  [ROUTES.WELCOME_SCREEN]: undefined;
  [ROUTES.DASHBOARD]: undefined;
  [ROUTES.PROJECT_DETAILS]: { fullPath: string };
  [ROUTES.ISSUE_DETAILS]: { fullPath: string; internalId: string };
};

export type WelcomeScreenNavigationProp = NativeStackNavigationProp<
  RootParamList,
  typeof ROUTES.WELCOME_SCREEN
>;

export type DashboardNavigationProp = NativeStackNavigationProp<
  RootParamList,
  typeof ROUTES.DASHBOARD
>;

export type ProjectDetailsNavigationProp = NativeStackNavigationProp<
  RootParamList,
  typeof ROUTES.PROJECT_DETAILS
>;

export type ProjectDetailsRouteProp = RouteProp<
  RootParamList,
  typeof ROUTES.PROJECT_DETAILS
>;

export type IssueDetailsNavigationProp = NativeStackNavigationProp<
  RootParamList,
  typeof ROUTES.ISSUE_DETAILS
>;

export type IssueDetailsRouteProp = RouteProp<
  RootParamList,
  typeof ROUTES.ISSUE_DETAILS
>;
