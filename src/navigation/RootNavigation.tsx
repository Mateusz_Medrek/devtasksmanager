import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StatusBar, useColorScheme } from 'react-native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';

import Dashboard from '../screens/Dashboard';
import IssueDetails from '../screens/IssueDetails';
import ProjectDetails from '../screens/ProjectDetails';
import WelcomeScreen from '../screens/WelcomeScreen';
import { useGitlabStore } from '../stores';
import { DarkTheme, LightTheme } from '../theme';
import { ROUTES } from './types';
import type { RootParamList } from './types';

interface Props {}

const RootStack = createNativeStackNavigator<RootParamList>();

const RootNavigation: React.FC<Props> = () => {
  const color = useColorScheme();
  const { accessToken, hydrate } = useGitlabStore();
  const [isHydrated, setIsHydrated] = useState(false);
  useEffect(() => {
    Promise.all([hydrate()])
      .then(() => {
        setIsHydrated(true);
      })
      .catch((err) => {
        console.log(err);
        setIsHydrated(true);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  if (!isHydrated) {
    // Here BootSplash will be displayed;
    return null;
  }
  return (
    <NavigationContainer theme={color === 'dark' ? DarkTheme : LightTheme}>
      <StatusBar
        animated={true}
        backgroundColor="transparent"
        barStyle={color !== 'dark' ? 'dark-content' : 'light-content'}
        translucent={true}
      />
      <RootStack.Navigator
        initialRouteName={
          accessToken ? ROUTES.DASHBOARD : ROUTES.WELCOME_SCREEN
        }
        screenOptions={{ headerShown: false, stackAnimation: 'fade' }}
      >
        <RootStack.Screen
          name={ROUTES.WELCOME_SCREEN}
          component={WelcomeScreen}
        />
        <RootStack.Screen name={ROUTES.DASHBOARD} component={Dashboard} />
        <RootStack.Screen
          name={ROUTES.PROJECT_DETAILS}
          component={ProjectDetails}
        />
        <RootStack.Screen
          name={ROUTES.ISSUE_DETAILS}
          component={IssueDetails}
          options={{ stackPresentation: 'transparentModal' }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
