export const ACTIVE_OPACITY = 0.4;
export const IPHONE11_HEIGHT = 896;
export const IPHONE11_WIDTH = 414;
export const ISSUE_TILE_WIDTH = 360;
export const PROJECT_TILE_HEIGHT = 100;
