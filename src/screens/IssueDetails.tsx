import { useLazyQuery, useQuery } from '@apollo/client';
import { useRoute, useTheme } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import CloseButton from '../components/CloseButton';
import LoadingIndicator from '../components/LoadingIndicator';
import IssueDetailsPager from '../components/IssueDetailsPager';
import IssueState from '../components/IssueState';
import {
  PROJECT_ISSUE_ASSIGNEES,
  PROJECT_ISSUE_DETAILS,
} from '../graphql/queries';
import type { IssueDetailsRouteProp } from '../navigation/types';
import type {
  ProjectIssueAssigneesQueryData,
  ProjectIssueAssigneesQueryVariables,
  ProjectIssueDetailsQueryData,
  ProjectIssueDetailsQueryVariables,
  User,
} from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';

const ASSIGNEES_INITIAL_PORTION = 30;
const SCREEN_MARGIN = 0.1;

interface Props {}

const IssueDetails: React.FC<Props> = () => {
  const route = useRoute<IssueDetailsRouteProp>();
  const [assignees, setAssignees] = useState<User[]>([]);
  const [assigneesPaginationId, setAssigneesPaginationId] = useState('');
  const [containerWidth, setContainerWidth] = useState(-1);
  const theme = useTheme();
  const { data, error, loading: queryLoading } = useQuery<
    ProjectIssueDetailsQueryData,
    ProjectIssueDetailsQueryVariables
  >(PROJECT_ISSUE_DETAILS, {
    variables: {
      fullPath: route.params.fullPath,
      iid: route.params.internalId,
    },
  });
  const [getAssignees, { data: assigneesData }] = useLazyQuery<
    ProjectIssueAssigneesQueryData,
    ProjectIssueAssigneesQueryVariables
  >(PROJECT_ISSUE_ASSIGNEES);
  useEffect(() => {
    !assignees.length &&
      getAssignees({
        variables: {
          fullPath: route.params.fullPath,
          iid: route.params.internalId,
          first: ASSIGNEES_INITIAL_PORTION,
          after: assigneesPaginationId,
        },
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (assigneesData && !!assigneesData.project.issue.assignees.edges.length) {
      setAssigneesPaginationId(
        assigneesData.project.issue.assignees.edges[
          assigneesData.project.issue.assignees.edges.length - 1
        ].cursor,
      );
      setAssignees((prevAssignees) => {
        const newAssignees = assigneesData.project.issue.assignees.edges
          .map(({ node }) => node)
          .filter((assignee) => {
            if (
              prevAssignees.find(
                (prevAssignee) => assignee.username === prevAssignee.username,
              )
            ) {
              return false;
            }
            return true;
          });
        return [...prevAssignees, ...newAssignees];
      });
      if (assigneesData.project.issue.assignees.pageInfo.hasNextPage) {
        getAssignees({
          variables: {
            fullPath: route.params.fullPath,
            iid: route.params.internalId,
            first: ASSIGNEES_INITIAL_PORTION,
            after:
              assigneesData.project.issue.assignees.edges[
                assigneesData.project.issue.assignees.edges.length - 1
              ].cursor,
          },
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [assigneesData]);
  const loading = queryLoading || containerWidth === -1;
  return (
    <View style={styles.wrapper}>
      <View style={{ flex: SCREEN_MARGIN }} />
      <View
        style={{
          flex: 1 - 2 * SCREEN_MARGIN,
          flexDirection: 'row',
          ...withShadow(1.55, theme.colors.border),
        }}
      >
        <View style={{ flex: SCREEN_MARGIN }} />
        <View
          onLayout={({
            nativeEvent: {
              layout: { width },
            },
          }) => {
            setContainerWidth(width);
          }}
          style={[
            styles.container,
            {
              flex: 1 - 2 * SCREEN_MARGIN,
              backgroundColor: theme.colors.background,
            },
          ]}
        >
          <View style={styles.headerContainer}>
            <CloseButton />
            <Text style={[styles.title, { color: theme.colors.text }]}>
              {data?.project.issue.title}
            </Text>
            {!!data?.project.issue.state && (
              <View style={styles.issueStateContainer}>
                <IssueState value={data?.project.issue.state} variant="small" />
              </View>
            )}
          </View>
          {loading || error ? (
            <View style={styles.loadingErrorContainer}>
              {loading && <LoadingIndicator />}
              {!!error && (
                <Text style={[styles.title, { color: theme.colors.text }]}>
                  {error.message}
                </Text>
              )}
            </View>
          ) : (
            <IssueDetailsPager
              assignees={assignees}
              fullPath={route.params.fullPath}
              internalId={route.params.internalId}
              issue={data?.project.issue}
              width={containerWidth}
            />
          )}
        </View>
        <View style={{ flex: SCREEN_MARGIN }} />
      </View>
      <View style={{ flex: SCREEN_MARGIN }} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 15,
    overflow: 'hidden',
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  issueStateContainer: {
    alignItems: 'flex-end',
    paddingHorizontal: getLayoutScaledSize(20),
  },
  loadingErrorContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  metadataContainer: {
    flex: 1,
  },
  title: {
    flex: 1,
    fontSize: getLayoutScaledSize(22),
    fontWeight: '700',
  },
  wrapper: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    flex: 1,
    justifyContent: 'center',
  },
});

export default IssueDetails;
