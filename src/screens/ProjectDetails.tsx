import { useQuery } from '@apollo/client';
import { useRoute, useTheme } from '@react-navigation/native';
import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import LoadingIndicator from '../components/LoadingIndicator';
import Header from '../components/Header';
import ProjectAvatar from '../components/ProjectAvatar';
import ProjectIssueList from '../components/ProjectIssueList';
import ProjectMetadata from '../components/ProjectMetadata';
import { PROJECT_DETAILS } from '../graphql/queries';
import useWindowDimensions from '../hooks/useWindowDimensions';
import type { ProjectDetailsRouteProp } from '../navigation/types';
import type {
  ProjectDetailsQueryData,
  ProjectDetailsQueryVariables,
} from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';

interface Props {}

const ProjectDetails: React.FC<Props> = () => {
  const route = useRoute<ProjectDetailsRouteProp>();
  const insets = useSafeAreaInsets();
  const theme = useTheme();
  const { width } = useWindowDimensions();
  const { data, error, loading } = useQuery<
    ProjectDetailsQueryData,
    ProjectDetailsQueryVariables
  >(PROJECT_DETAILS, { variables: { fullPath: route.params.fullPath } });
  return (
    <View style={[styles.wrapper, { paddingBottom: insets.bottom }]}>
      <Header applySafeInsetTop />
      <View style={styles.container}>
        {loading ? (
          <LoadingIndicator />
        ) : error ? (
          <Text style={[styles.errorText, { color: theme.colors.text }]}>
            {error?.message}
          </Text>
        ) : (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: 'space-between',
            }}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{ width }}
          >
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                padding: getLayoutScaledSize(20),
              }}
            >
              <ProjectAvatar
                avatarUrl={data?.project.avatarUrl}
                borderColor={theme.colors.notification}
                variant="large"
              />
            </View>
            <View style={{ alignItems: 'center' }}>
              <ProjectMetadata project={data?.project} />
            </View>
            <View style={{ alignItems: 'center' }}>
              <ProjectIssueList fullPath={route.params.fullPath} />
            </View>
          </ScrollView>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  errorText: {
    fontSize: getLayoutScaledSize(30),
    fontWeight: '700',
  },
  wrapper: {
    alignItems: 'center',
    flex: 1,
  },
});

export default ProjectDetails;
