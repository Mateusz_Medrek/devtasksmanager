import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import GitlabIcon from '../components/GitlabIcon';
import { ROUTES } from '../navigation/types';
import type { WelcomeScreenNavigationProp } from '../navigation/types';
import GitlabService from '../services/GitlabService';
import { useGitlabStore } from '../stores';
import defaultTranslations from '../translations/defaultTranslations';
import withShadow from '../utils/withShadow';

const LOGO_SIZE = 190;

interface Props {}

const WelcomeScreen: React.FC<Props> = () => {
  const navigation = useNavigation<WelcomeScreenNavigationProp>();
  const insets = useSafeAreaInsets();
  const theme = useTheme();
  const { setTokens } = useGitlabStore();
  const authorize = async () => {
    try {
      const tokens = await GitlabService.authorize();
      setTokens(tokens);
      navigation.navigate(ROUTES.DASHBOARD);
    } catch (err) {
      // console.log(err);
    }
  };
  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: theme.colors.background,
          paddingTop: insets.top,
          paddingBottom: insets.bottom,
        },
      ]}
    >
      <View style={{ alignItems: 'center', flex: 1 }}>
        <View
          style={{
            alignItems: 'center',
            flex: 0.3,
            justifyContent: 'center',
            overflow: 'hidden',
            padding: 5,
          }}
        >
          <View
            style={[
              styles.logo,
              withShadow(1.55, theme.colors.border),
              { backgroundColor: 'white' },
            ]}
          />
        </View>
        <View
          style={{ alignItems: 'center', flex: 0.1, justifyContent: 'center' }}
        >
          <Text style={[styles.appName, { color: theme.colors.text }]}>
            {defaultTranslations.welcomeScreen.appName}
          </Text>
        </View>
        <View
          style={{ alignItems: 'center', flex: 0.2, justifyContent: 'center' }}
        >
          <Text style={[styles.welcome, { color: theme.colors.text }]}>
            {defaultTranslations.welcomeScreen.welcome}
          </Text>
        </View>
        <View
          style={{ alignItems: 'center', flex: 0.1, justifyContent: 'center' }}
        >
          <Text style={[styles.chooseProvider, { color: theme.colors.text }]}>
            {defaultTranslations.welcomeScreen.chooseProvider}
          </Text>
        </View>
        <View style={{ flex: 0.2 }}>
          <GitlabIcon onPress={authorize} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  appName: {
    fontSize: 36,
    fontWeight: '700',
  },
  chooseProvider: {
    fontSize: 24,
    fontWeight: '400',
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  logo: {
    borderRadius: LOGO_SIZE / 2,
    height: LOGO_SIZE,
    width: LOGO_SIZE,
  },
  welcome: {
    fontSize: 48,
    fontWeight: 'bold',
  },
});

export default WelcomeScreen;
