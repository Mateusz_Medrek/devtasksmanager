import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import UserData from '../components/UserData';
import ProjectList from '../components/ProjectList';
import useDisabledBackPress from '../hooks/useDisabledBackPress';

interface Props {}

const Dashboard: React.FC<Props> = () => {
  useDisabledBackPress();
  const insets = useSafeAreaInsets();
  const theme = useTheme();
  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: theme.colors.background,
          paddingBottom: insets.bottom,
          paddingTop: insets.top,
        },
      ]}
    >
      <View style={{ alignItems: 'center', flex: 0.15 }}>
        <UserData testID="UserDataID" />
      </View>
      <View style={{ alignItems: 'center', flex: 0.85 }}>
        <ProjectList testID="ProjectListID" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

export default Dashboard;
