import type { Theme } from '@react-navigation/native';

const LightTheme: Theme = {
  dark: false,
  colors: {
    background: '#D2C3C4',
    border: '#FF2453',
    card: '#FFFCFC',
    notification: '#FF459E',
    primary: '#FF346A',
    text: '#0E1C36',
  },
};

export default LightTheme;
