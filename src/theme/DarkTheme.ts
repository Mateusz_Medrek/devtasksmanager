import type { Theme } from '@react-navigation/native';

const DarkTheme: Theme = {
  dark: true,
  colors: {
    background: '#292929',
    border: '#0D2818',
    card: '#058C42',
    notification: '#16DB65',
    primary: '#04471C',
    text: '#CFCFCF',
  },
};

export default DarkTheme;
