import create from 'zustand';

import GitlabService from '../services/GitlabService';

export interface GitlabState {
  accessToken: string;
  refreshToken: string;
  hydrate: () => Promise<void>;
  setTokens: (tokens: { access_token: string; refresh_token: string }) => void;
}

const [useGitlabStore] = create<GitlabState>((set) => ({
  accessToken: '',
  refreshToken: '',
  hydrate: async () => {
    const tokens = await GitlabService.getTokens();
    set(tokens);
  },
  setTokens: async (tokens) => {
    const tokensObject = {
      accessToken: tokens.access_token,
      refreshToken: tokens.refresh_token,
    };
    set(tokensObject);
    GitlabService.persistTokens(tokensObject);
  },
}));

export default useGitlabStore;
