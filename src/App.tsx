import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  createHttpLink,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import RootNavigation from './navigation/RootNavigation';
import GitlabService from './services/GitlabService';

interface Props {}

const gitlabApiLink = createHttpLink({
  uri: 'https://gitlab.com/api/graphql',
});

const authLink = setContext(async (_, { headers }) => {
  const tokens = await GitlabService.getTokens();
  return {
    headers: {
      ...headers,
      Authorization: `Bearer ${tokens.accessToken}`,
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(gitlabApiLink),
  cache: new InMemoryCache(),
});

const App: React.FC<Props> = () => {
  return (
    <ApolloProvider client={client}>
      <SafeAreaProvider>
        <RootNavigation />
      </SafeAreaProvider>
    </ApolloProvider>
  );
};

export default App;
