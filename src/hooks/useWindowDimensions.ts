import { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import type { ScaledSize } from 'react-native';

const useWindowDimensions = () => {
  const [window, setWindow] = useState(Dimensions.get('window'));
  useEffect(() => {
    const handleDimensions = ({ window: w }: { window: ScaledSize }) => {
      setWindow(w);
    };
    setWindow(Dimensions.get('window'));
    Dimensions.addEventListener('change', handleDimensions);
    return () => Dimensions.removeEventListener('change', handleDimensions);
  }, []);
  return window;
};

export default useWindowDimensions;
