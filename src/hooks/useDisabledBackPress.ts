import { useFocusEffect } from '@react-navigation/native';
import { useCallback } from 'react';
import { BackHandler } from 'react-native';

const useDisabledBackPress = () => {
  const handleBackPress = useCallback(() => {
    /** Notify react-navigation that back press is already handled */
    const onBackPress = () => true;
    BackHandler.addEventListener('hardwareBackPress', onBackPress);
    return () =>
      BackHandler.removeEventListener('hardwareBackPress', onBackPress);
  }, []);
  useFocusEffect(handleBackPress);
};

export default useDisabledBackPress;
