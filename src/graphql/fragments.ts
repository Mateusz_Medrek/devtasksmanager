import { gql } from '@apollo/client';

export const USER_DATA_FRAGMENT = gql`
  fragment UserData on User {
    name
    username
  }
`;
