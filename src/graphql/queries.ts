import { gql } from '@apollo/client';

import { USER_DATA_FRAGMENT } from './fragments';

export const GET_CURRENT_USER = gql`
  query GetCurrentUser {
    currentUser {
      avatarUrl
      name
      username
    }
  }
`;

export const PROJECTS = gql`
  query Projects($first: Int, $after: String) {
    projects(membership: true, first: $first, after: $after) {
      edges {
        cursor
        node {
          avatarUrl
          fullPath
          id
          name
        }
      }
      pageInfo {
        hasNextPage
      }
    }
  }
`;

export const PROJECT_DETAILS = gql`
  query ProjectDetails($fullPath: ID!) {
    project(fullPath: $fullPath) {
      avatarUrl
      description
      id
      name
      nameWithNamespace
      openIssuesCount
      visibility
    }
  }
`;

export const PROJECT_ISSUES = gql`
  query ProjectIssues(
    $fullPath: ID!
    $first: Int
    $after: String
    $sort: IssueSort
  ) {
    project(fullPath: $fullPath) {
      id
      issues(first: $first, after: $after, sort: $sort) {
        edges {
          cursor
          node {
            id
            iid
            milestone {
              title
            }
            state
            taskCompletionStatus {
              completedCount
              count
            }
            title
          }
        }
        pageInfo {
          hasNextPage
        }
      }
    }
  }
`;

export const PROJECT_ISSUE_ASSIGNEES = gql`
  query ProjectIssueAssignees(
    $fullPath: ID!
    $iid: String
    $first: Int
    $after: String
  ) {
    project(fullPath: $fullPath) {
      id
      issue(iid: $iid) {
        assignees(first: $first, after: $after) {
          edges {
            cursor
            node {
              ...UserData
            }
          }
          pageInfo {
            hasNextPage
          }
        }
        id
      }
    }
  }
  ${USER_DATA_FRAGMENT}
`;

export const PROJECT_ISSUE_DETAILS = gql`
  query ProjectIssueDetails($fullPath: ID!, $iid: String) {
    project(fullPath: $fullPath) {
      id
      issue(iid: $iid) {
        author {
          ...UserData
        }
        description
        id
        iid
        milestone {
          description
          title
        }
        state
        taskCompletionStatus {
          completedCount
          count
        }
        title
      }
    }
  }
  ${USER_DATA_FRAGMENT}
`;

export const PROJECT_ISSUE_DISCUSSIONS = gql`
  query ProjectIssueDiscussions(
    $fullPath: ID!
    $iid: String
    $firstDiscussions: Int
    $afterDiscussion: String
    $firstNotes: Int
    $afterNote: String
  ) {
    project(fullPath: $fullPath) {
      id
      issue(iid: $iid) {
        discussions(first: $firstDiscussions, after: $afterDiscussion) {
          edges {
            cursor
            node {
              createdAt
              id
              notes(first: $firstNotes, after: $afterNote) {
                edges {
                  cursor
                  node {
                    author {
                      ...UserData
                    }
                    body
                    createdAt
                    id
                  }
                }
                pageInfo {
                  hasNextPage
                }
              }
              resolved
            }
          }
          pageInfo {
            hasNextPage
          }
        }
        id
      }
    }
  }
  ${USER_DATA_FRAGMENT}
`;
