import AsyncStorage from '@react-native-community/async-storage';

import getDeepLinkURL from '../utils/getDeepLinkURL';
import parseURLQueryToJSON from '../utils/parseURLQueryToJSON';

export type Tokens = { accessToken: string; refreshToken: string };

class GitlabService {
  static TOKENS_PERSISTENCE_KEY = 'devtasksmanager-gitlab-tokens';
  static async authorize() {
    const APP_ID =
      'b3e9d4511b629dda477bd074826c136c50f5fb33c489d66404efe719e975e828';
    const APP_SECRET =
      '271f7f581619667babb24f09cd634350111e980b8f4a7a0c066363169e1938a7';
    const REDIRECT_URI = encodeURI('devtasksmanager://gitlab.auth');
    const STATE = 'xyz';
    const SCOPES =
      'api+read_user+read_api+read_repository+write_repository+read_registry+write_registry+sudo+openid+profile+email';
    const url = `https://gitlab.com/oauth/authorize?client_id=${APP_ID}&redirect_uri=${REDIRECT_URI}&response_type=code&state=${STATE}&scope=${SCOPES}`;
    try {
      const callbackUrl = await getDeepLinkURL(url);
      const queryParams = parseURLQueryToJSON(new URL(callbackUrl));
      if (!queryParams.code || queryParams.state !== STATE) {
        throw new Error('Request malformed');
      }
      const response = await fetch(
        `https://gitlab.com/oauth/token?client_id=${APP_ID}&client_secret=${APP_SECRET}&code=${queryParams.code}&grant_type=authorization_code&redirect_uri=${REDIRECT_URI}`,
        { method: 'POST' },
      );
      const json = await response.json();
      return Promise.resolve(json);
    } catch (err) {
      console.warn('Cancelled auth flow', err);
      return Promise.reject(err);
    }
  }
  static async getTokens(): Promise<Tokens> {
    const stringifiedTokens = await AsyncStorage.getItem(
      GitlabService.TOKENS_PERSISTENCE_KEY,
    );
    if (stringifiedTokens === null) {
      return { accessToken: '', refreshToken: '' };
    }
    return JSON.parse(stringifiedTokens);
  }
  static async persistTokens(tokens: Tokens) {
    return AsyncStorage.setItem(
      GitlabService.TOKENS_PERSISTENCE_KEY,
      JSON.stringify(tokens),
    );
  }
}

export default GitlabService;
