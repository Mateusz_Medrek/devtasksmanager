import { useTheme } from '@react-navigation/native';
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';

import getLayoutScaledSize from '../utils/getLayoutScaledSize';

type Props = {
  avatarUrl?: string | null;
  borderColor?: string;
  variant?: 'small' | 'large';
};

const RATIO = 0.69;
const SMALL_SIZE = getLayoutScaledSize(50);
const LARGE_SIZE = getLayoutScaledSize(200);

const ProjectAvatar: React.FC<Props> & {
  SIZE: { SMALL: number; LARGE: number };
} = ({ avatarUrl, borderColor, variant = 'small' }) => {
  const theme = useTheme();
  return (
    <View
      style={[
        styles.container,
        variant === 'small' && styles.containerSmall,
        variant === 'large' && styles.containerLarge,
        { borderColor: borderColor || theme.colors.border },
      ]}
    >
      {!avatarUrl ? (
        <Ionicon
          accessibilityLabel="Project avatar icon"
          name="git-compare"
          color={theme.colors.primary}
          size={(variant === 'large' ? LARGE_SIZE : SMALL_SIZE) * RATIO}
        />
      ) : (
        <Image
          accessibilityLabel="Project avatar icon"
          source={{ uri: avatarUrl }}
          style={[
            variant === 'small' && styles.imageSmall,
            variant === 'large' && styles.imageLarge,
          ]}
        />
      )}
    </View>
  );
};

ProjectAvatar.SIZE = {
  SMALL: SMALL_SIZE,
  LARGE: LARGE_SIZE,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    overflow: 'hidden',
  },
  containerSmall: {
    borderRadius: SMALL_SIZE / 2,
    height: SMALL_SIZE,
    width: SMALL_SIZE,
  },
  containerLarge: {
    borderRadius: LARGE_SIZE / 2,
    height: LARGE_SIZE,
    width: LARGE_SIZE,
  },
  imageSmall: {
    height: SMALL_SIZE * RATIO,
    width: SMALL_SIZE * RATIO,
  },
  imageLarge: {
    height: LARGE_SIZE * RATIO,
    width: LARGE_SIZE * RATIO,
  },
});

export default ProjectAvatar;
