import { useTheme } from '@react-navigation/native';
import React, { useRef } from 'react';
import {
  Platform,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import { ACTIVE_OPACITY } from '../consts/styles';
import defaultTranslations from '../translations/defaultTranslations';
import { IssueDetails, User } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';
import IssueDiscussionList from './IssueDiscussionList';
import IssueMetadata from './IssueMetadata';

interface Props {
  assignees: User[];
  fullPath: string;
  internalId: string;
  issue?: IssueDetails;
  width: number;
}

const IssueDetailsPager: React.FC<Props> = ({
  assignees,
  fullPath,
  internalId,
  issue,
  width,
}) => {
  const theme = useTheme();
  const pagerRef = useRef<ScrollView>(null);
  return (
    <>
      <View style={[styles.pagerButtons, { width }]}>
        <Pressable
          android_disableSound={true}
          android_ripple={{
            color: theme.colors.notification,
            borderless: false,
          }}
          onPress={() => {
            pagerRef?.current?.scrollTo({ x: 0, animated: true });
          }}
          style={({ pressed }) => [
            styles.pagerButton,
            {
              backgroundColor: theme.colors.primary,
              borderColor: theme.colors.border,
            },
            withShadow(2.55, theme.colors.border),
            Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
          ]}
        >
          <Text style={[styles.pagerButtonText, { color: theme.colors.text }]}>
            {defaultTranslations.issueDetailsPager.detailsButton}
          </Text>
        </Pressable>
        <Pressable
          android_disableSound={true}
          android_ripple={{
            color: theme.colors.notification,
            borderless: false,
          }}
          onPress={() => {
            pagerRef?.current?.scrollTo({
              x: width,
              animated: true,
            });
          }}
          style={({ pressed }) => [
            styles.pagerButton,
            {
              backgroundColor: theme.colors.primary,
              borderColor: theme.colors.border,
            },
            withShadow(2.55, theme.colors.border),
            Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
          ]}
        >
          <Text style={[styles.pagerButtonText, { color: theme.colors.text }]}>
            {defaultTranslations.issueDetailsPager.discussionsButton}
          </Text>
        </Pressable>
      </View>
      <ScrollView
        ref={pagerRef}
        contentContainerStyle={{
          alignItems: 'center',
        }}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        <View style={{ width }}>
          <IssueMetadata assignees={assignees} issue={issue} />
        </View>
        <View style={{ width }}>
          <IssueDiscussionList fullPath={fullPath} internalId={internalId} />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  pagerButton: {
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    flex: 0.5,
    height: getLayoutScaledSize(80),
    justifyContent: 'center',
  },
  pagerButtonText: {
    fontSize: getLayoutScaledSize(26),
    fontWeight: 'bold',
  },
  pagerButtons: {
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default IssueDetailsPager;
