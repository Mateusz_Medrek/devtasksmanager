import { useLazyQuery } from '@apollo/client';
import { useTheme } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { ISSUE_TILE_WIDTH } from '../consts/styles';
import { PROJECT_ISSUES } from '../graphql/queries';
import useWindowDimensions from '../hooks/useWindowDimensions';
import type {
  Issue,
  ProjectIssuesQueryData,
  ProjectIssuesQueryVariables,
} from '../types/GitlabTypes';
import IssueTile from './IssueTile';
import LoadingIndicator from './LoadingIndicator';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';

interface Props {
  fullPath: string;
}

const INITIAL_ISSUES_PORTION = 30;

const ProjectIssueList: React.FC<Props> = ({ fullPath }) => {
  const theme = useTheme();
  const { width } = useWindowDimensions();
  const [issues, setIssues] = useState<Issue[]>([]);
  const [paginationId, setPaginationId] = useState('');
  const [getIssues, { data, loading }] = useLazyQuery<
    ProjectIssuesQueryData,
    ProjectIssuesQueryVariables
  >(PROJECT_ISSUES);
  useEffect(() => {
    !issues.length &&
      getIssues({
        variables: {
          fullPath,
          first: INITIAL_ISSUES_PORTION,
          after: paginationId,
          sort: 'updated_desc',
        },
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (data && !!data.project.issues.edges.length) {
      setPaginationId(
        data.project.issues.edges[data.project.issues.edges.length - 1].cursor,
      );
      setIssues((prevIssues) => {
        const newProjects = data.project.issues.edges
          .map(({ node }) => node)
          .filter((issue) => {
            if (prevIssues.find((prevIssue) => issue.id === prevIssue.id)) {
              return false;
            }
            return true;
          });
        return [...prevIssues, ...newProjects];
      });
    }
  }, [data]);
  return (
    <View style={styles.container}>
      {!!issues.length && (
        <FlatList
          ListFooterComponent={() => {
            if (!data?.project.issues.pageInfo.hasNextPage) {
              return null;
            }
            return (
              <View
                style={[
                  styles.footerContainer,
                  { backgroundColor: theme.colors.card },
                  withShadow(1.55, theme.colors.border),
                ]}
              >
                <LoadingIndicator />
              </View>
            );
          }}
          data={issues}
          decelerationRate="fast"
          horizontal
          keyExtractor={(item) => item.id}
          onEndReached={() => {
            data?.project.issues.pageInfo.hasNextPage &&
              getIssues({
                variables: {
                  fullPath,
                  first: INITIAL_ISSUES_PORTION,
                  after: paginationId,
                  sort: 'updated_desc',
                },
              });
          }}
          onEndReachedThreshold={0.1}
          refreshing={loading}
          renderItem={({ item }) => {
            return <IssueTile fullPath={fullPath} issue={item} />;
          }}
          snapToAlignment="start"
          snapToInterval={getLayoutScaledSize(ISSUE_TILE_WIDTH)}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={[{ width }]}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexGrow: 1,
    justifyContent: 'center',
  },
  footerContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    width: getLayoutScaledSize(ISSUE_TILE_WIDTH / 3),
  },
});

export default ProjectIssueList;
