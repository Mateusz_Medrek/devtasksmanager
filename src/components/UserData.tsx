import { useQuery } from '@apollo/client';
import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { GET_CURRENT_USER } from '../graphql/queries';
import useWindowDimensions from '../hooks/useWindowDimensions';
import type { CurrentUserQueryData } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import GitlabIcon from './GitlabIcon';
import LoadingIndicator from './LoadingIndicator';
import UserAvatar from './UserAvatar';

interface Props {
  testID?: string;
}

const UserData: React.FC<Props> = () => {
  const { height, width } = useWindowDimensions();
  const isLandscape = width > height;
  const theme = useTheme();
  const { data, error, loading } = useQuery<CurrentUserQueryData>(
    GET_CURRENT_USER,
  );
  return (
    <View
      style={[styles.container, { backgroundColor: theme.colors.background }]}
    >
      {loading ? (
        <LoadingIndicator />
      ) : error ? (
        <Text style={{ color: theme.colors.primary }}>{error.message}</Text>
      ) : (
        <>
          <View style={{ alignItems: 'center', flex: 0.25 }}>
            <UserAvatar
              avatarUrl={data?.currentUser?.avatarUrl}
              borderColor={theme.colors.notification}
              variant={isLandscape ? 'superSmall' : 'small'}
            />
          </View>
          <View
            style={{
              alignItems: isLandscape ? 'center' : 'flex-start',
              flex: 0.5,
              flexDirection: isLandscape ? 'row' : 'column',
            }}
          >
            <Text style={[styles.name, { color: theme.colors.text }]}>
              {data?.currentUser?.name}
            </Text>
            <Text style={[styles.username, { color: theme.colors.text }]}>
              {`@${data?.currentUser?.username}`}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
              flex: 0.25,
              justifyContent: 'center',
            }}
          >
            <GitlabIcon
              disabled
              size={
                isLandscape
                  ? getLayoutScaledSize(105)
                  : getLayoutScaledSize(150)
              }
            />
          </View>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  name: {
    fontSize: getLayoutScaledSize(36),
    fontWeight: '700',
    marginHorizontal: getLayoutScaledSize(5),
  },
  username: {
    fontSize: getLayoutScaledSize(24),
    fontWeight: '400',
    marginHorizontal: getLayoutScaledSize(5),
  },
});

export default UserData;
