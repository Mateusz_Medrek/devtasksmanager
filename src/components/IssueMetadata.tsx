import { useTheme } from '@react-navigation/native';
import Color from 'color';
import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

import defaultTranslations from '../translations/defaultTranslations';
import type { IssueDetails, User } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import GradientBlur from './GradientBlur';
import IssueAssignee from './IssueAssignee';

interface Props {
  assignees: User[];
  issue?: IssueDetails;
}

const BLUR_HEIGHT = 10;
const BLUR_LOCATIONS = [0.1, 0.3, 0.5, 0.7, 0.9];

const IssueMetadata: React.FC<Props> = ({ assignees, issue }) => {
  const theme = useTheme();
  const bottomBlurColors = BLUR_LOCATIONS.map((location) =>
    Color(theme.colors.background).alpha(location).string(),
  );
  const topBlurColors = BLUR_LOCATIONS.map((location) =>
    Color(theme.colors.background)
      .alpha(1 - location)
      .string(),
  );
  if (!issue) {
    return null;
  }
  return (
    <View style={styles.container}>
      <GradientBlur
        colors={topBlurColors}
        height={BLUR_HEIGHT}
        locations={BLUR_LOCATIONS}
        variant="top"
      />
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          paddingVertical: BLUR_HEIGHT,
        }}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.metadata}>
          <Text
            style={[
              styles.label,
              styles.metadataLabel,
              { color: theme.colors.text },
            ]}
          >
            {defaultTranslations.issueDetails.author}
          </Text>
          <Text
            style={[
              styles.value,
              styles.metadataValue,
              { color: theme.colors.text },
            ]}
          >
            {issue.author.name}
          </Text>
        </View>
        <View style={styles.metadata}>
          <Text
            style={[
              styles.label,
              styles.metadataLabel,
              { color: theme.colors.text },
            ]}
          >
            {defaultTranslations.issueDetails.assignees}
          </Text>
          <Text style={[styles.value, styles.metadataValue]}>
            {assignees.map((assignee, i) => (
              <View key={i}>
                <IssueAssignee assignee={assignee.name} />
              </View>
            ))}
          </Text>
        </View>
        <View style={styles.metadata}>
          <Text
            style={[
              styles.label,
              styles.metadataLabel,
              { color: theme.colors.text },
            ]}
          >
            {defaultTranslations.issueDetails.description}
          </Text>
          <View style={styles.value}>
            <Text style={[styles.metadataValue, { color: theme.colors.text }]}>
              {issue.description}
            </Text>
          </View>
        </View>
        {issue.milestone && (
          <>
            <View style={styles.metadata}>
              <Text
                style={[
                  styles.label,
                  styles.metadataLabel,
                  { color: theme.colors.text },
                ]}
              >
                {defaultTranslations.issueDetails.milestone}
              </Text>
              <View style={styles.value}>
                <Text
                  style={[styles.metadataValue, { color: theme.colors.text }]}
                >
                  {issue.milestone.title}
                </Text>
                {!!issue.milestone.description && (
                  <Text
                    style={[styles.metadataValue, { color: theme.colors.text }]}
                  >
                    {issue.milestone.description}
                  </Text>
                )}
              </View>
            </View>
          </>
        )}
        {!!issue.taskCompletionStatus.count && (
          <View style={styles.metadata}>
            <Text
              style={[
                styles.label,
                styles.metadataLabel,
                { color: theme.colors.text },
              ]}
            >
              {defaultTranslations.issueDetails.tasks}
            </Text>
            <Text
              style={[
                styles.value,
                styles.metadataValue,
                { color: theme.colors.text },
              ]}
            >
              {issue.taskCompletionStatus.completedCount +
                '/' +
                issue.taskCompletionStatus.count}
            </Text>
          </View>
        )}
      </ScrollView>
      <GradientBlur
        colors={bottomBlurColors}
        height={BLUR_HEIGHT}
        locations={BLUR_LOCATIONS}
        variant="bottom"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  label: {
    flex: 0.18,
  },
  metadata: {
    alignItems: 'center',
    flexDirection: 'row',
    margin: getLayoutScaledSize(10),
  },
  metadataLabel: {
    alignSelf: 'flex-start',
    fontSize: getLayoutScaledSize(18),
    fontWeight: '300',
    padding: getLayoutScaledSize(10),
  },
  metadataValue: {
    fontSize: getLayoutScaledSize(24),
    fontWeight: '500',
    padding: getLayoutScaledSize(10),
  },
  value: {
    flex: 0.82,
  },
});

export default IssueMetadata;
