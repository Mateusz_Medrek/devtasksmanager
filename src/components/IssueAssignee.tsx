import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import getLayoutScaledSize from '../utils/getLayoutScaledSize';

interface Props {
  assignee: string;
}

const IssueAssignee: React.FC<Props> = ({ assignee }) => {
  const theme = useTheme();
  return (
    <View style={styles.container}>
      <Text style={[styles.assignee, { color: theme.colors.text }]}>
        {assignee}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  assignee: {
    fontSize: getLayoutScaledSize(18),
    fontWeight: '500',
  },
  container: {
    alignItems: 'center',
    borderRadius: getLayoutScaledSize(18),
    backgroundColor: 'gray',
    height: getLayoutScaledSize(48),
    justifyContent: 'center',
    padding: getLayoutScaledSize(10),
  },
});

export default IssueAssignee;
