import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View } from 'react-native';

import type { Discussion } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import IssueDiscussionNote from './IssueDiscussionNote';

interface Props {
  discussion: Discussion;
}

const TILE_MARGIN = 0.05;

const IssueDiscussion: React.FC<Props> = ({ discussion }) => {
  const theme = useTheme();
  return (
    <View
      style={[styles.container, { backgroundColor: theme.colors.background }]}
    >
      <View style={{ flex: TILE_MARGIN }} />
      <View style={{ flex: 1 - 2 * TILE_MARGIN }}>
        {discussion.notes.edges.map(({ node: note }, i) => (
          <IssueDiscussionNote key={note.id} isReply={i > 0} note={note} />
        ))}
      </View>
      <View style={{ flex: TILE_MARGIN }} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: getLayoutScaledSize(8),
  },
});

export default IssueDiscussion;
