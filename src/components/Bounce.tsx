import React from 'react';
import { StyleSheet } from 'react-native';
import Animated, {
  Clock,
  Easing,
  Value,
  add,
  block,
  cond,
  clockRunning,
  eq,
  interpolate,
  not,
  set,
  startClock,
  timing,
  useCode,
  useValue,
  greaterThan,
} from 'react-native-reanimated';

export const delay = (node: Animated.Node<number>, delayValue: number) => {
  const clock = new Clock();
  const delayed = new Value<number>(0);
  return block([
    cond(clockRunning(clock), 0, [
      startClock(clock),
      cond(eq(delayed, 0), set(delayed, add(delayValue, clock))),
    ]),
    cond(greaterThan(clock, delayed), node),
  ]);
};

export const bounce = ({
  bounceDuration = 2000,
  toValue = 100,
  delayValue = 0,
}) => {
  const { clock, easing, duration, from, to } = {
    clock: new Clock(),
    duration: bounceDuration,
    from: 0,
    to: toValue,
    easing: Easing.bezier(0.42, 0.0, 0.58, 1.0),
  };
  const state: Animated.TimingState = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0),
  };
  const config = {
    toValue: new Value(0),
    duration,
    easing,
  };
  return block([
    cond(not(clockRunning(clock)), [
      delay(
        block([
          set(config.toValue, to),
          set(state.frameTime, 0),
          set(state.finished, 0),
          set(state.time, 0),
          set(state.position, from),
          startClock(clock),
        ]),
        delayValue,
      ),
    ]),
    timing(clock, state, config),
    cond(state.finished, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.frameTime, 0),
      set(config.toValue, cond(config.toValue, 0, toValue)),
    ]),
    state.position,
  ]);
};

const Bounce: React.FC = () => {
  const first = useValue<number>(0);
  const second = useValue<number>(0);
  useCode(
    () =>
      block([
        set(first, bounce({})),
        set(second, bounce({ delayValue: 1000 })),
      ]),
    [],
  );
  return (
    <Animated.View style={styles.container}>
      <Animated.View
        style={[
          styles.circle,
          {
            transform: [
              {
                scale: interpolate(first, {
                  inputRange: [0, 45, 55, 100],
                  outputRange: [0.01, 1, 1, 0.01],
                }),
              },
            ],
          },
        ]}
      />
      <Animated.View
        style={[
          styles.circle,
          {
            transform: [
              {
                scale: interpolate(second, {
                  inputRange: [0, 45, 55, 100],
                  outputRange: [0.01, 1, 1, 0.01],
                }),
              },
            ],
          },
        ]}
      />
    </Animated.View>
  );
};

const SIZE = 300;

const styles = StyleSheet.create({
  circle: {
    backgroundColor: 'green',
    borderRadius: SIZE / 2,
    height: SIZE,
    opacity: 0.6,
    position: 'absolute',
    width: SIZE,
  },
  container: {
    backgroundColor: 'yellow',
    height: SIZE,
    width: SIZE,
  },
});

export default Bounce;
