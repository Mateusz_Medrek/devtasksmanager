import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import type { IssueStateValue } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';

interface Props {
  value: IssueStateValue;
  variant?: 'small' | 'large';
}

const IssueState: React.FC<Props> = ({ value, variant = 'large' }) => {
  const backgroundColor =
    value === 'opened'
      ? 'green'
      : value === 'closed'
      ? 'red'
      : value === 'locked'
      ? 'gray'
      : 'blue';
  return (
    <View
      style={[
        styles.container,
        { backgroundColor },
        variant === 'large' && styles.largeContainer,
        variant === 'small' && styles.smallContainer,
      ]}
    >
      <Text
        style={[
          styles.value,
          variant === 'large' && styles.largeValue,
          variant === 'small' && styles.smallValue,
        ]}
      >
        {value}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  largeContainer: {
    borderRadius: getLayoutScaledSize(18),
    padding: getLayoutScaledSize(16),
  },
  largeValue: {
    fontSize: getLayoutScaledSize(20),
  },
  smallContainer: {
    borderRadius: getLayoutScaledSize(12),
    padding: getLayoutScaledSize(10),
  },
  smallValue: {
    fontSize: getLayoutScaledSize(14),
  },
  value: {
    color: 'white',
    textTransform: 'capitalize',
  },
});

export default IssueState;
