import { useLazyQuery } from '@apollo/client';
import { useTheme } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { PROJECT_TILE_HEIGHT } from '../consts/styles';
import { PROJECTS } from '../graphql/queries';
import useWindowDimensions from '../hooks/useWindowDimensions';
import type {
  Project,
  ProjectsQueryData,
  ProjectsQueryVariables,
} from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import LoadingIndicator from './LoadingIndicator';
import ProjectTile from './ProjectTile';

const INITIAL_PROJECTS_PORTION = 30;
const FOOTER_MARGIN = 0.3;

interface Props {
  testID?: string;
}

const ProjectList: React.FC<Props> = () => {
  const { width } = useWindowDimensions();
  const theme = useTheme();
  const [projects, setProjects] = useState<Project[]>([]);
  const [paginationId, setPaginationId] = useState('');
  const [getProjects, { data, loading }] = useLazyQuery<
    ProjectsQueryData,
    ProjectsQueryVariables
  >(PROJECTS);
  useEffect(() => {
    !projects.length &&
      getProjects({
        variables: { first: INITIAL_PROJECTS_PORTION, after: paginationId },
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (data && !!data.projects.edges.length) {
      setPaginationId(
        data.projects.edges[data.projects.edges.length - 1].cursor,
      );
      setProjects((prevProjects) => {
        const newProjects = data.projects.edges
          .map(({ node }) => node)
          .filter((project) => {
            if (
              prevProjects.find((prevProject) => project.id === prevProject.id)
            ) {
              return false;
            }
            return true;
          });
        return [...prevProjects, ...newProjects];
      });
    }
  }, [data]);
  return (
    <View style={styles.container}>
      <FlatList
        ListFooterComponent={() => {
          if (!data?.projects.pageInfo.hasNextPage) {
            return null;
          }
          return (
            <View style={styles.footerContainer}>
              <View style={{ flex: FOOTER_MARGIN }} />
              <View style={{ flex: 1 - 2 * FOOTER_MARGIN }}>
                <LoadingIndicator style={styles.footerStyle} />
              </View>
              <View style={{ flex: FOOTER_MARGIN }} />
            </View>
          );
        }}
        data={projects}
        decelerationRate="fast"
        keyExtractor={(item) => item.id}
        onEndReached={() => {
          data?.projects.pageInfo.hasNextPage &&
            getProjects({
              variables: {
                first: INITIAL_PROJECTS_PORTION,
                after: paginationId,
              },
            });
        }}
        onEndReachedThreshold={0.1}
        refreshing={loading}
        renderItem={({ item }) => {
          return <ProjectTile project={item} />;
        }}
        snapToAlignment="end"
        snapToInterval={getLayoutScaledSize(PROJECT_TILE_HEIGHT)}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: theme.colors.card, width }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1 },
  footerContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    height: getLayoutScaledSize(PROJECT_TILE_HEIGHT),
  },
  footerStyle: {
    height: getLayoutScaledSize(PROJECT_TILE_HEIGHT / 2),
  },
  separator: {
    height: StyleSheet.hairlineWidth,
  },
});

export default ProjectList;
