import { useTheme } from '@react-navigation/native';
import React from 'react';
import { Platform, Pressable, StyleSheet, Text, View } from 'react-native';
import type { PressableProps, StyleProp, ViewStyle } from 'react-native';

import { ACTIVE_OPACITY } from '../consts/styles';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';

interface Props extends Omit<PressableProps, 'children'> {
  color?: string;
  style?: StyleProp<ViewStyle>;
  title?: string;
}

const Button: React.FC<Props> = (props) => {
  const theme = useTheme();
  const { color, style, title, ...rest } = props;
  return (
    <Pressable
      android_disableSound={true}
      android_ripple={{
        color: theme.colors.primary,
        borderless: false,
      }}
      {...rest}
      style={({ pressed }) => [
        styles.container,
        Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
        withShadow(1.55, theme.colors.primary),
        StyleSheet.flatten(style),
      ]}
    >
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.1 }} />
        <View style={[styles.container, { flex: 0.8 }]}>
          <Text style={[styles.title, { color }]}>{title ?? ''}</Text>
        </View>
        <View style={{ flex: 0.1 }} />
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: getLayoutScaledSize(14),
    fontWeight: 'bold',
  },
});

export default Button;
