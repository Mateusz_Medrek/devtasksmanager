import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';

import defaultTranslations from '../translations/defaultTranslations';
import type { ProjectDetails } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';

const HORIZONTAL_MARGIN = 0.15;

interface Props {
  project?: ProjectDetails;
}

const ProjectMetadata: React.FC<Props> = ({ project }) => {
  const theme = useTheme();
  return (
    <View style={styles.container}>
      <View style={{ flex: 0.5, flexDirection: 'row' }}>
        <View style={{ flex: HORIZONTAL_MARGIN }} />
        <View style={{ flex: 1 - 2 * HORIZONTAL_MARGIN }}>
          <View style={styles.projectNameContainer}>
            <Text style={[styles.projectName, { color: theme.colors.text }]}>
              {project?.name}
            </Text>
            {!!project?.visibility && (
              <View style={styles.projectVisibility}>
                <Ionicon
                  name={
                    project.visibility === 'private'
                      ? 'lock-closed'
                      : 'lock-open'
                  }
                  color={theme.colors.primary}
                  size={getLayoutScaledSize(30)}
                />
              </View>
            )}
          </View>
          <View style={styles.projectNameContainer}>
            <Text
              style={[
                styles.projectNameWithNamespace,
                { color: theme.colors.text },
              ]}
            >
              {project?.nameWithNamespace}
            </Text>
          </View>
        </View>
        <View style={{ flex: HORIZONTAL_MARGIN }} />
      </View>
      <View style={{ flex: 0.5, flexDirection: 'row' }}>
        <View style={{ flex: HORIZONTAL_MARGIN }} />
        <View style={{ flex: 1 - 2 * HORIZONTAL_MARGIN }}>
          <View style={styles.projectMetadata}>
            <Text
              style={[
                styles.projectMetadataLabel,
                { color: theme.colors.notification },
              ]}
            >
              {defaultTranslations.projectDetails.id}
            </Text>
            <Text
              style={[
                styles.projectMetadataValue,
                { color: theme.colors.text },
              ]}
            >
              {project?.id}
            </Text>
          </View>
          <View style={styles.projectMetadata}>
            <Text
              style={[
                styles.projectMetadataLabel,
                { color: theme.colors.notification },
              ]}
            >
              {defaultTranslations.projectDetails.openIssues}
            </Text>
            <Text
              style={[
                styles.projectMetadataValue,
                { color: theme.colors.text },
              ]}
            >
              {project?.openIssuesCount}
            </Text>
          </View>
          <View style={styles.projectMetadata}>
            <View>
              <Text
                style={[
                  styles.projectMetadataLabel,
                  { color: theme.colors.notification },
                ]}
              >
                {defaultTranslations.projectDetails.description}
              </Text>
              <Text
                style={[
                  styles.projectMetadataValue,
                  { color: theme.colors.text },
                ]}
              >
                {project?.description ||
                  defaultTranslations.projectDetails.noDescription}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ flex: HORIZONTAL_MARGIN }} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  metadataContainer: {
    paddingVertical: getLayoutScaledSize(24),
  },
  projectMetadata: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: getLayoutScaledSize(24),
  },
  projectMetadataLabel: {
    fontSize: getLayoutScaledSize(24),
    fontWeight: '200',
  },
  projectMetadataValue: {
    fontSize: getLayoutScaledSize(24),
    fontWeight: '800',
  },
  projectName: {
    flex: 0.85,
    fontSize: getLayoutScaledSize(46),
    fontWeight: 'bold',
  },
  projectNameContainer: {
    alignItems: 'center',
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: getLayoutScaledSize(24),
  },
  projectNameWithNamespace: {
    fontSize: getLayoutScaledSize(30),
    fontStyle: 'italic',
  },
  projectVisibility: {
    alignItems: 'center',
    flex: 0.15,
  },
});

export default ProjectMetadata;
