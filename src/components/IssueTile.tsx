import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { Platform, Pressable, StyleSheet, Text, View } from 'react-native';

import IssueState from '../components/IssueState';
import { ACTIVE_OPACITY, ISSUE_TILE_WIDTH } from '../consts/styles';
import { ROUTES } from '../navigation/types';
import type { ProjectDetailsNavigationProp } from '../navigation/types';
import type { Issue } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';

const TILE_HORIZONTAL_MARGIN = 0.05;
const TILE_VERTICAL_MARGIN = 0.1;

interface Props {
  fullPath: string;
  issue: Issue;
}

const IssueTile: React.FC<Props> = ({ fullPath, issue }) => {
  const navigation = useNavigation<ProjectDetailsNavigationProp>();
  const theme = useTheme();
  return (
    <Pressable
      accessibilityLabel={`${issue.title} issue`}
      android_ripple={{ color: theme.colors.notification, borderless: false }}
      onPress={() =>
        navigation.navigate(ROUTES.ISSUE_DETAILS, {
          fullPath,
          internalId: issue.iid,
        })
      }
      style={({ pressed }) => [
        styles.wrapper,
        { backgroundColor: theme.colors.card },
        withShadow(1.55, theme.colors.border),
        Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
      ]}
    >
      <View style={{ flex: TILE_HORIZONTAL_MARGIN }} />
      <View style={{ flex: 1 - 2 * TILE_HORIZONTAL_MARGIN }}>
        <View style={{ flex: TILE_VERTICAL_MARGIN }} />
        <View style={{ flex: 1 - 2 * TILE_VERTICAL_MARGIN }}>
          <View style={{ flex: 0.5 }}>
            <Text
              numberOfLines={2}
              style={[styles.title, { color: theme.colors.text }]}
            >
              {issue.title}
            </Text>
          </View>
          <View style={{ flex: 0.2 }}>
            <Text
              numberOfLines={1}
              style={[styles.milestone, { color: theme.colors.text }]}
            >
              {issue.milestone?.title}
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
              flex: 0.3,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <IssueState value={issue.state} />
            {!!issue.taskCompletionStatus.count && (
              <Text style={[styles.milestone, { color: theme.colors.text }]}>
                {issue.taskCompletionStatus.completedCount +
                  '/' +
                  issue.taskCompletionStatus.count}
              </Text>
            )}
          </View>
        </View>
        <View style={{ flex: TILE_VERTICAL_MARGIN }} />
      </View>
      <View style={{ flex: TILE_HORIZONTAL_MARGIN }} />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  milestone: {
    fontSize: getLayoutScaledSize(18),
    fontWeight: '400',
  },
  title: {
    flex: 1,
    fontSize: getLayoutScaledSize(24),
    fontWeight: 'bold',
  },
  wrapper: {
    aspectRatio: 1.2,
    flexDirection: 'row',
    width: getLayoutScaledSize(ISSUE_TILE_WIDTH),
  },
});

export default IssueTile;
