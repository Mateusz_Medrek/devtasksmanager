import React from 'react';
import { useTheme } from '@react-navigation/native';
import { ActivityIndicator } from 'react-native';
import type { StyleProp, ViewStyle } from 'react-native';

type Props = {
  style?: StyleProp<ViewStyle>;
};

const LoadingIndicator: React.FC<Props> = ({ style }) => {
  const theme = useTheme();
  return (
    <ActivityIndicator
      color={theme.colors.primary}
      size="large"
      style={style}
      testID="LoadingIndicatorID"
    />
  );
};

export default LoadingIndicator;
