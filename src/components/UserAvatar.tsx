import { useTheme } from '@react-navigation/native';
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';

import getLayoutScaledSize from '../utils/getLayoutScaledSize';

type Props = {
  avatarUrl?: string | null;
  borderColor?: string;
  variant?: 'superSmall' | 'small' | 'large';
};

const RATIO = 0.69;
const SUPER_SMALL_SIZE = getLayoutScaledSize(105);
const SMALL_SIZE = getLayoutScaledSize(150);
const LARGE_SIZE = getLayoutScaledSize(300);

const UserAvatar: React.FC<Props> & {
  SIZE: { SMALL: number; LARGE: number };
} = ({ avatarUrl, borderColor, variant = 'small' }) => {
  const theme = useTheme();
  return (
    <View
      style={[
        styles.container,
        variant === 'superSmall' && styles.containerSuperSmall,
        variant === 'small' && styles.containerSmall,
        variant === 'large' && styles.containerLarge,
        { borderColor: borderColor || theme.colors.border },
      ]}
    >
      {!avatarUrl ? (
        <Ionicon
          accessibilityRole="image"
          name="person"
          color={theme.colors.primary}
          size={
            (variant === 'large'
              ? LARGE_SIZE
              : variant === 'superSmall'
              ? SUPER_SMALL_SIZE
              : SMALL_SIZE) * RATIO
          }
        />
      ) : (
        <Image
          accessibilityRole="image"
          source={{ uri: avatarUrl }}
          style={[
            variant === 'superSmall' && styles.imageSuperSmall,
            variant === 'small' && styles.imageSmall,
            variant === 'large' && styles.imageLarge,
          ]}
        />
      )}
    </View>
  );
};

UserAvatar.SIZE = {
  SMALL: SMALL_SIZE,
  LARGE: LARGE_SIZE,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    overflow: 'hidden',
  },
  containerSuperSmall: {
    borderRadius: SUPER_SMALL_SIZE / 2,
    height: SUPER_SMALL_SIZE,
    width: SUPER_SMALL_SIZE,
  },
  containerSmall: {
    borderRadius: SMALL_SIZE / 2,
    height: SMALL_SIZE,
    width: SMALL_SIZE,
  },
  containerLarge: {
    borderRadius: LARGE_SIZE / 2,
    height: LARGE_SIZE,
    width: LARGE_SIZE,
  },
  imageSuperSmall: {
    height: SUPER_SMALL_SIZE,
    width: SUPER_SMALL_SIZE,
  },
  imageSmall: {
    height: SMALL_SIZE,
    width: SMALL_SIZE,
  },
  imageLarge: {
    height: LARGE_SIZE,
    width: LARGE_SIZE,
  },
});

export default UserAvatar;
