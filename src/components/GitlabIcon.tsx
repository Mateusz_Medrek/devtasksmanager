import React from 'react';
import { Platform, Pressable, StyleSheet } from 'react-native';
import type { PressableProps, StyleProp, ViewStyle } from 'react-native';
import FontistoIcon from 'react-native-vector-icons/Fontisto';

import { ACTIVE_OPACITY } from '../consts/styles';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';

const DEFAULT_SIZE = getLayoutScaledSize(180);
const GitlabOrange = '#FC6D27';
const GitlabDarkOrange = '#E2432A';

interface Props extends Omit<PressableProps, 'children' | 'style'> {
  style?: StyleProp<ViewStyle>;
  size?: number;
}

const GitlabIcon: React.FC<Props> = (props) => {
  const { size = DEFAULT_SIZE } = props;
  return (
    <Pressable
      accessibilityRole="imagebutton"
      accessibilityState={{ disabled: !!props.disabled }}
      android_disableSound={true}
      android_ripple={{
        color: GitlabDarkOrange,
        borderless: true,
        radius: size / 2,
      }}
      {...props}
      style={({ pressed }) => [
        styles.container,
        props.style,
        { borderRadius: size / 2, height: size, width: size },
        Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
      ]}
    >
      <FontistoIcon name="gitlab" color={GitlabDarkOrange} size={size * 0.6} />
    </Pressable>
  );
};

GitlabIcon.defaultProps = {
  size: DEFAULT_SIZE,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: GitlabOrange,
    borderColor: GitlabDarkOrange,
    borderWidth: 2,
    justifyContent: 'center',
  },
});

export default GitlabIcon;
