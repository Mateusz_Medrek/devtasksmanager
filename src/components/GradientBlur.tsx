import React from 'react';
import { StyleSheet, View } from 'react-native';
import type { StyleProp, ViewStyle } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import type { LinearGradientProps } from 'react-native-linear-gradient';

interface Props {
  colors?: LinearGradientProps['colors'];
  height?: number;
  locations?: LinearGradientProps['locations'];
  /** If height is provided in style, it overrides height prop */
  style?: StyleProp<ViewStyle>;
  variant?: 'bottom' | 'top';
}

const GradientBlur: React.FC<Props> & { top: 'top'; bottom: 'bottom' } = ({
  colors = [
    'rgba(255,255,255,0.8)',
    'rgba(255,255,255,0.5)',
    'rgba(255,255,255,0.2)',
  ],
  height = 10,
  locations = [0.25, 0.5, 0.75],
  style,
  variant = 'top',
}) => {
  return (
    <View
      pointerEvents="none"
      style={[
        styles.container,
        { height },
        variant === 'bottom' && styles.bottom,
        variant === 'top' && styles.top,
        style,
      ]}
    >
      <LinearGradient
        colors={colors}
        locations={locations}
        style={styles.gradient}
      />
    </View>
  );
};

GradientBlur.bottom = 'bottom';
GradientBlur.top = 'top';

const styles = StyleSheet.create({
  bottom: {
    bottom: 0,
  },
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 10,
  },
  gradient: {
    ...StyleSheet.absoluteFillObject,
  },
  top: {
    top: 0,
  },
});

export default GradientBlur;
