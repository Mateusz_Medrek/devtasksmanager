import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { Platform, Pressable, StyleSheet, Text, View } from 'react-native';

import { ACTIVE_OPACITY, PROJECT_TILE_HEIGHT } from '../consts/styles';
import { ROUTES } from '../navigation/types';
import type { ProjectDetailsNavigationProp } from '../navigation/types';
import defaultTranslations from '../translations/defaultTranslations';
import type { Project } from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';
import ProjectAvatar from './ProjectAvatar';

const TILE_MARGIN_HORIZONTAL = 0.07;
const TILE_MARGIN_VERTICAL = 0.05;

interface Props {
  project: Project;
}

const ProjectTile: React.FC<Props> = ({ project }) => {
  const navigation = useNavigation<ProjectDetailsNavigationProp>();
  const theme = useTheme();
  return (
    <Pressable
      accessibilityLabel={`${project.name} project`}
      android_ripple={{ color: theme.colors.notification, borderless: false }}
      onPress={() =>
        navigation.navigate(ROUTES.PROJECT_DETAILS, {
          fullPath: project.fullPath,
        })
      }
      style={({ pressed }) => [
        styles.wrapper,
        withShadow(1.55, theme.colors.border),
        { backgroundColor: theme.colors.card },
        Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
      ]}
    >
      <View style={{ flex: TILE_MARGIN_VERTICAL }} />
      <View
        style={[styles.container, { flex: 1 - 2 * TILE_MARGIN_HORIZONTAL }]}
      >
        <View style={{ flex: TILE_MARGIN_HORIZONTAL }} />
        <ProjectAvatar avatarUrl={project.avatarUrl} variant="small" />
        <View style={{ flex: TILE_MARGIN_HORIZONTAL }} />
        <View style={styles.metadata}>
          <Text
            numberOfLines={1}
            style={[
              styles.name,
              styles.nameWeight,
              { color: theme.colors.text },
            ]}
          >
            {project.name}
          </Text>
          <Text style={[styles.name, { color: theme.colors.text }]}>
            <Text style={styles.idLabel}>
              {defaultTranslations.projectTile.id}
            </Text>
            <Text style={styles.idValue}>{project.id}</Text>
          </Text>
        </View>
        <View style={{ flex: TILE_MARGIN_HORIZONTAL }} />
      </View>
      <View style={{ flex: TILE_MARGIN_VERTICAL }} />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  idLabel: {
    fontSize: getLayoutScaledSize(18),
    fontWeight: '200',
  },
  idValue: {
    flex: 1,
    fontSize: getLayoutScaledSize(18),
    fontWeight: '600',
  },
  metadata: {
    flex: 1,
    paddingHorizontal: getLayoutScaledSize(18),
  },
  name: {
    flex: 1,
    fontSize: getLayoutScaledSize(30),
  },
  nameWeight: {
    fontWeight: 'bold',
  },
  wrapper: {
    flex: 1,
    height: getLayoutScaledSize(PROJECT_TILE_HEIGHT),
  },
});

export default ProjectTile;
