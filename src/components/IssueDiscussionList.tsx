import { useLazyQuery } from '@apollo/client';
import { useTheme } from '@react-navigation/native';
import Color from 'color';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import { PROJECT_ISSUE_DISCUSSIONS } from '../graphql/queries';
import type {
  Discussion,
  ProjectIssueDiscussionsQueryData,
  ProjectIssueDiscussionsQueryVariables,
} from '../types/GitlabTypes';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import GradientBlur from './GradientBlur';
import IssueDiscussion from './IssueDiscussion';

const INITIAL_DISCUSSIONS_PORTION = 50;
/** @todo implement pagination for notes */
const INITIAL_NOTES_PORTION = 50;

const BLUR_HEIGHT = 10;
const BLUR_LOCATIONS = [0.1, 0.3, 0.5, 0.7, 0.9];

interface Props {
  fullPath: string;
  internalId: string;
}

const IssueDiscussionList: React.FC<Props> = ({ fullPath, internalId }) => {
  const theme = useTheme();
  const bottomBlurColors = BLUR_LOCATIONS.map((location) =>
    Color(theme.colors.background).alpha(location).string(),
  );
  const topBlurColors = BLUR_LOCATIONS.map((location) =>
    Color(theme.colors.background)
      .alpha(1 - location)
      .string(),
  );
  const [discussions, setDiscussions] = useState<Discussion[]>([]);
  const [discussionPaginationId, setDiscussionPaginationId] = useState('');
  const [getDiscussions, { data }] = useLazyQuery<
    ProjectIssueDiscussionsQueryData,
    ProjectIssueDiscussionsQueryVariables
  >(PROJECT_ISSUE_DISCUSSIONS);
  useEffect(() => {
    !discussions.length &&
      getDiscussions({
        variables: {
          fullPath,
          iid: internalId,
          firstDiscussions: INITIAL_DISCUSSIONS_PORTION,
          afterDiscussion: discussionPaginationId,
          firstNotes: INITIAL_NOTES_PORTION,
          afterNote: '',
        },
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (data && !!data.project.issue.discussions.edges.length) {
      setDiscussions((prevDiscussions) => {
        const newDiscussions = data.project.issue.discussions.edges
          .map(({ node }) => node)
          .filter((discussion) => {
            if (
              prevDiscussions.find(
                (prevDiscussion) => prevDiscussion.id === discussion.id,
              )
            ) {
              return false;
            }
            return true;
          });
        return [...prevDiscussions, ...newDiscussions];
      });
      setDiscussionPaginationId(
        data.project.issue.discussions.edges[
          data.project.issue.discussions.edges.length - 1
        ].cursor,
      );
    }
  }, [data]);
  if (!discussions.length) {
    return null;
  }
  return (
    <View style={styles.container}>
      <GradientBlur
        colors={topBlurColors}
        height={BLUR_HEIGHT}
        locations={BLUR_LOCATIONS}
        variant="top"
      />
      <FlatList
        contentContainerStyle={{
          paddingVertical: BLUR_HEIGHT,
        }}
        data={discussions}
        keyExtractor={(item) => item.id}
        onEndReached={() => {
          data?.project.issue.discussions.pageInfo.hasNextPage &&
            getDiscussions({
              variables: {
                fullPath,
                iid: internalId,
                firstDiscussions: INITIAL_DISCUSSIONS_PORTION,
                afterDiscussion: discussionPaginationId,
                firstNotes: INITIAL_NOTES_PORTION,
                afterNote: '',
              },
            });
        }}
        onEndReachedThreshold={0.1}
        renderItem={({ item }) => {
          return <IssueDiscussion discussion={item} />;
        }}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      />
      <GradientBlur
        colors={bottomBlurColors}
        height={BLUR_HEIGHT}
        locations={BLUR_LOCATIONS}
        style={{ bottom: -1 }}
        variant="bottom"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default IssueDiscussionList;
