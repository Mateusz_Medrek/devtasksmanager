import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { Platform, Pressable, StyleSheet } from 'react-native';
import Animated from 'react-native-reanimated';

import { ACTIVE_OPACITY } from '../consts/styles';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';

const CloseButton: React.FC = () => {
  const navigation = useNavigation();
  const theme = useTheme();
  return (
    <Pressable
      accessibilityLabel="Close button"
      android_disableSound={true}
      android_ripple={{
        color: theme.colors.primary,
        borderless: true,
      }}
      onPress={() => navigation.goBack()}
      style={({ pressed }) => [
        styles.container,
        Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
      ]}
    >
      <Animated.View
        style={[
          styles.crossElement,
          {
            backgroundColor: theme.colors.notification,
            transform: [{ rotate: '-45deg' }],
          },
        ]}
      />
      <Animated.View
        style={[
          styles.crossElement,
          {
            backgroundColor: theme.colors.notification,
            transform: [{ rotate: '45deg' }],
          },
        ]}
      />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: getLayoutScaledSize(80),
    justifyContent: 'center',
    width: getLayoutScaledSize(80),
  },
  crossElement: {
    position: 'absolute',
    height: getLayoutScaledSize(50),
    width: getLayoutScaledSize(5),
  },
});

export default CloseButton;
