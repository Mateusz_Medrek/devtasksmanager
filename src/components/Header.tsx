import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { Platform, Pressable, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Ionicon from 'react-native-vector-icons/Ionicons';

import { ACTIVE_OPACITY } from '../consts/styles';
import defaultTranslations from '../translations/defaultTranslations';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';

const SIDE_AREA = 0.15;

interface Props {
  applySafeInsetTop?: boolean;
}

const Header: React.FC<Props> = ({ applySafeInsetTop = true }) => {
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const theme = useTheme();
  return (
    <View
      style={[
        styles.container,
        { backgroundColor: theme.colors.card },
        applySafeInsetTop && { paddingTop: insets.top },
      ]}
    >
      <View
        style={{
          alignItems: 'center',
          flex: SIDE_AREA,
          justifyContent: 'center',
        }}
      >
        <Pressable
          accessibilityLabel="Back button"
          android_disableSound={true}
          android_ripple={{
            color: theme.colors.primary,
            borderless: true,
          }}
          onPress={() => {
            if (!navigation.canGoBack()) {
              return;
            }
            navigation.goBack();
          }}
          style={({ pressed }) => [
            Platform.OS === 'ios' && pressed && { opacity: ACTIVE_OPACITY },
          ]}
        >
          <View
            style={[
              styles.container,
              { paddingVertical: getLayoutScaledSize(20) },
            ]}
          >
            <Ionicon
              name="chevron-back"
              color={theme.colors.notification}
              size={getLayoutScaledSize(20)}
            />
            <Text
              style={[styles.backTitle, { color: theme.colors.notification }]}
            >
              {defaultTranslations.header.back}
            </Text>
          </View>
        </Pressable>
      </View>
      <View style={{ flex: 1 - 2 * SIDE_AREA }}></View>
      <View style={{ flex: SIDE_AREA }}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  backTitle: {
    fontSize: getLayoutScaledSize(18),
    fontWeight: '700',
  },
});

export default Header;
