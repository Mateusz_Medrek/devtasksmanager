import { useTheme } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import type { Note } from '../types/GitlabTypes';
import dateToLocaleString from '../utils/dateToLocaleString';
import getLayoutScaledSize from '../utils/getLayoutScaledSize';
import withShadow from '../utils/withShadow';

interface Props {
  isReply: boolean;
  note: Note;
}

const IssueDiscussionNote: React.FC<Props> = ({ isReply, note }) => {
  const theme = useTheme();
  const [createdAt, setCreatedAt] = useState(new Date(note.createdAt));
  useEffect(() => {
    setCreatedAt(new Date(note.createdAt));
  }, [note.createdAt]);
  return (
    <View style={[withShadow(1.55, theme.colors.border)]}>
      <View
        style={[
          styles.container,
          { backgroundColor: theme.colors.card },
          isReply && styles.replyContainer,
        ]}
      >
        <View style={{ flex: 0.75 }}>
          <Text style={[styles.bodyText, { color: theme.colors.text }]}>
            {note.body}
          </Text>
        </View>
        <View style={{ flex: 0.25 }}>
          <Text style={[styles.createdAtText, { color: theme.colors.text }]}>
            {dateToLocaleString(createdAt)}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bodyText: {
    fontSize: getLayoutScaledSize(24),
  },
  container: {
    borderRadius: getLayoutScaledSize(20),
    flexDirection: 'row',
    overflow: 'hidden',
    padding: getLayoutScaledSize(20),
  },
  createdAtText: {
    alignSelf: 'flex-end',
    fontSize: getLayoutScaledSize(16),
    fontWeight: '400',
  },
  replyContainer: {
    marginLeft: getLayoutScaledSize(20),
  },
});

export default IssueDiscussionNote;
