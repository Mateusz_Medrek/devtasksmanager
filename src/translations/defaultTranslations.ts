const defaultTranslations = {
  header: {
    back: 'Back',
  },
  issueDetails: {
    assignees: 'Assignees: ',
    author: 'Author: ',
    description: 'Description: ',
    milestone: 'Milestone: ',
    tasks: 'Tasks: ',
  },
  issueDetailsPager: {
    detailsButton: 'Details',
    discussionsButton: 'Discussions',
  },
  projectDetails: {
    description: 'Description: ',
    id: 'Project ID: ',
    noDescription: 'No description',
    openIssues: 'Open issues: ',
  },
  projectList: {
    footerButtonTitle: 'Fetch more projects',
  },
  projectTile: {
    id: 'Project ID: ',
  },
  welcomeScreen: {
    appName: 'devtasksmanager',
    chooseProvider: 'Choose provider',
    gitlab: 'Gitlab',
    welcome: 'Welcome',
  },
};

export default defaultTranslations;
