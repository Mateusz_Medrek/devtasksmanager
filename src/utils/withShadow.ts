import type { ViewStyle } from 'react-native';

const withShadow = (elevation = 1, shadowColor = 'black'): ViewStyle => {
  return {
    elevation,
    shadowColor,
    shadowOffset: {
      width: 0,
      height: elevation - 1,
    },
    shadowOpacity: 0.88,
    shadowRadius: elevation,
  };
};

export default withShadow;
