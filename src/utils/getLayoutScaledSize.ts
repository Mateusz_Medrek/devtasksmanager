import { Dimensions } from 'react-native';

import { IPHONE11_HEIGHT, IPHONE11_WIDTH } from '../consts/styles';

/**
 * Returns size multiplied by current device's aspectRatio divided by iPhone 11's aspectRatio
 */
const getLayoutScaledSize = (size: number) => {
  const { height, width } = Dimensions.get('window');
  const IPHONE11_ASPECT_RATIO = IPHONE11_HEIGHT / IPHONE11_WIDTH;
  const aspectRatio = height > width ? height / width : width / height;
  return size * (aspectRatio / IPHONE11_ASPECT_RATIO);
};

export default getLayoutScaledSize;
