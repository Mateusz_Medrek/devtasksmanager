const parseURLQueryToJSON = (url: URL) => {
  return url.search
    .replace('?', '')
    .split('&')
    .map((keyValue) => {
      const [key, value] = keyValue.split('=');
      return {
        [key]: value,
      };
    })
    .reduce(
      (prev, curr) => ({
        ...prev,
        ...curr,
      }),
      {},
    );
};

export default parseURLQueryToJSON;
